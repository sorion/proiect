<?php
namespace app\controllers;
use \framework\BaseController as BaseController;
use app\models\ArticleModel as ArticleModel;
use framework\FlashMessages as Mess;

class ArticleController extends BaseController{
	// o proprietate privata care contine modelul
	private $_model;

	// conectarea la model - daca este cazul
	public function __construct()
	{
		$this->_model = new ArticleModel;			
	}

	public function listAction()
	{
		$articles = $this->_model->listArticle();	
		$this->render('list', array('articles' => $articles));
	}
	
	public function readAction()
	{
		$article = $this->_model->findByPk('articles');	
		$this->render('read', array('article' => $article));
	}

	public function editAction()
	{
		// method from parent Model class (must have id parameter in URL)
		$article = $this->_model->findByPk('articles');
		if(isset($_POST['Article'])){
			if($this->_model->update((int)$_GET['id'], $_POST['Article'])){
				Mess::setMess('success', 'Update cu succes!');
				$this->redirect('index.php?c=article&a=list');
			}
		}

		$this->render('update', array('article' => $article));

	}

	public function addAction()
	{
		if($this->_model->insert($_POST['Article'])){
			Mess::setMess('success', 'Insert cu succes!');
			$this->redirect('index.php?c=article&a=list');
		}
		
		$this->render('update');
	}

	public function deleteAction()
	{
		if(!empty($_GET['id']) && isset($_POST['delete'])){
			$this->_model->delete('articles', $_GET['id']);
			Mess::setMess('success', 'Delete cu succes!');
			$this->redirect('index.php?c=article&a=list');
		}		
		
		$this->render('delete', (int)$_GET['id']);
	}	
}