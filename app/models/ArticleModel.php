<?php
namespace app\models;
use framework\Model as Model;
use framework\FlashMessages as Mess;

class ArticleModel extends Model
{
	// PDO connection - aceasta linie trebuie sa fie in fiecare model
	private $pdo;

	// suprascrierea constructorului din parinte - linia asta trebuie sa fie in fiecare model
	public function __construct(){
		parent::__construct();
		$this->pdo = parent::$_pdo;
	}	

	private function validate($post)
	{		
		$validate = true;
		if(empty($post['titlu'])){
			Mess::setMess('titlu', 'Invalid titlu');
			$validate = false;
		}	
		if(empty($post['continut'])){
			Mess::setMess('continut', 'Invalid continut');
			$validate = false;
		}
		if(empty($post['autor']) || !ctype_alpha($post['autor'])){
			Mess::setMess('autor', 'Invalid autor');
			$validate = false;
		}	

		return $validate;
	}

	// Get a list of articles
	public function listArticle()
	{
		return $this->pdo->query('SELECT id, titlu, CONCAT(MID(continut,1,200), " ...") AS continut, autor, data_creare, data_update FROM articles');
	}

	public function update($id, $post)
	{			
		if(!$this->validate($post)){			
			return false;			
		}

		$set = 'titlu = :titlu, continut = :continut, autor = :autor, data_update = CURDATE()';
		$sql = $this->pdo->prepare("UPDATE articles SET $set WHERE id = :id");

		return $sql->execute(array(
			':id' => $id, 
			':titlu' => $post['titlu'],
			':continut' => $post['continut'],
			':autor' => $post['autor']));				
	}
	
	public function insert($post)
	{
		if(!isset($post) || !$this->validate($post)){
			return false;
		}

		$sql = $this->pdo->prepare("INSERT INTO articles VALUES (NULL, :titlu, :continut, :autor, CURDATE(), '')");

		return $sql->execute(array(
			':titlu' => $post['titlu'],
			':continut' => $post['continut'],
			':autor' => $post['autor']));
	}
}