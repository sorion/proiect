<?php
namespace framework;
use framework\Framework as Framework;

class Model
{	
	// database connection instance	- singleton
	static protected $_pdo;	

	public function __construct()
	{
		$this->connectDB();
	}

	/**
	* Singleton pattern
	* Daca, de exemplu, intr-o pagina avem mai multe apelari ale Model-ului, doar o instanta de PDO este creata
	* e.g listare un articol si sub el comentarii - se apeleaza modelul de la articole si cel de la comentarii 
	*/ 
	protected function connectDB()
	{
		if(is_null(self::$_pdo)){
			$host = Framework::$params['database']['host'];
			$database = Framework::$params['database']['name'];
			$username = Framework::$params['database']['username'];	
			$password = Framework::$params['database']['password'];		
			$options = Framework::$params['database']['options'];

			self::$_pdo = new \PDO("mysql:dbname={$database};host={$host};port=3306;charset=utf8", $username, $password, $options);
		}else{
			return self::$_pdo;
		}				
	}

	public function findByPk($table)
	{
		if(isset($_GET['id']) && ctype_digit($_GET['id'])){
			$id = (int)$_GET['id'];
			$sql = self::$_pdo->prepare("SELECT * FROM $table WHERE id = :id");	
			$sql->execute(array(':id' => $id));	

			$result = $sql->fetch();	

			if(empty($result)){
				throw new \Exception('ID inexistent');
			}
		}
		return $result;
	}

	public function delete($table, $id){		
		$sql = self::$_pdo->prepare("DELETE FROM $table WHERE id = :id");	
		$sql->execute(array(':id' => $id));
	}
}